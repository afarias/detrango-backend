package br.com.detrango.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.detrango.business.TipoItemBO;
import br.com.detrango.exceptin.BusinessException;
import br.com.detrango.model.TipoItem;
import br.com.detrango.model.filter.TipoItemFilter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

@RestController
@CrossOrigin
@RequestMapping(value = "/tipoItem")
@Api(value = "TipoItem", description = "Controller de Tipo de Itens", tags = "TipoItem")
public class TipoItemController {
	
	@Autowired
	private TipoItemBO tipoItemBO;

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@RequestMapping(value = "/salvar", method = RequestMethod.POST)
	@ApiOperation(value = "Endpoint para cadastrar Tipo de Item.", authorizations = { @Authorization(value="apiKey") })
	public ResponseEntity<TipoItem> salvar(@RequestBody TipoItem tipoItem) throws BusinessException {
		try {
			return new ResponseEntity<>(tipoItemBO.salvar(tipoItem), HttpStatus.CREATED);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@RequestMapping(value = "/listar", method = RequestMethod.GET)
	@ApiOperation(value = "Endpoint para listar os Tipos de Itens", authorizations = { @Authorization(value="apiKey") })
	public ResponseEntity<Page<TipoItem>> listar(TipoItemFilter filter, Pageable pageble) throws BusinessException {		
		try {
			return new ResponseEntity<>(tipoItemBO.listar(filter, pageble), HttpStatus.OK);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@ApiOperation(value = "Endpoint para editar Tipo de iten", authorizations = { @Authorization(value="apiKey") })
	@PutMapping("/editar/{id}")
	public ResponseEntity<TipoItem> editar(@PathVariable("id") long id, @RequestBody TipoItem tipoItem) throws BusinessException {				
		try {
			
			Optional<TipoItem> tipoItemAtual = tipoItemBO.findById(id);

			if (tipoItemAtual.isPresent()) {
				TipoItem _tipoitem = tipoItemAtual.get();
				_tipoitem.setDescricao(tipoItem.getDescricao());
				_tipoitem.setTipoItem(tipoItem.getTipoItem());
				return new ResponseEntity<>(tipoItemBO.salvar(_tipoitem), HttpStatus.OK);
			} else {
				throw new BusinessException("Tipo de item não encontrado.");
			}
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}		
	}
	
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@RequestMapping(value = "/excluir/{id}", method = RequestMethod.DELETE)
	@ApiOperation(value = "Endpoint para excluir Tipo de Item", authorizations = { @Authorization(value="apiKey") })
	public ResponseEntity<HttpStatus> excluir(@PathVariable("id") Long id) throws BusinessException {
		try {
			tipoItemBO.apagar(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
}
