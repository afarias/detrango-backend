package br.com.detrango.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.detrango.model.request.LoginRequest;
import br.com.detrango.model.request.SignupRequest;
import br.com.detrango.service.jwt.JwtUserDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin
@RequestMapping(value = "/authentication")
@Api(value = "Autenticação", description = "Controller de Autenticação", tags = "Autenticação")
public class AuthenticationController {		

	@Autowired
	private JwtUserDetailsService userDetailsService;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ApiOperation(value = "Endpoit para obter o usuário Json Web Token de autenticação da API.")
	public ResponseEntity<?> createAuthenticationToken(@Valid @RequestBody LoginRequest loginRequest) throws Exception {	
		return ResponseEntity.ok(userDetailsService.login(loginRequest));
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	@ApiOperation(value = "Endpoit para criar usuário de utilização da API.")
	public ResponseEntity<?> saveUser(@Valid @RequestBody SignupRequest signUpRequest) throws Exception {
		try {
			return new ResponseEntity<>(userDetailsService.save(signUpRequest), HttpStatus.CREATED);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}	
	}

}