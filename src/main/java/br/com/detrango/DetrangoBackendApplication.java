package br.com.detrango;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DetrangoBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(DetrangoBackendApplication.class, args);
	}

}
