package br.com.detrango.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import br.com.detrango.model.TipoItem;

public interface TipoItemRepository extends PagingAndSortingRepository<TipoItem, Long> , JpaSpecificationExecutor<TipoItem>{

}
