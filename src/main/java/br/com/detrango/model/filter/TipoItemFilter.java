package br.com.detrango.model.filter;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import br.com.detrango.model.TipoItem;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TipoItemFilter {

	private Long tipoItemId;
	private String tipoItem;
	private String descricao;

	public Specification<TipoItem> toSpec() {
		return (root, query, builder) -> {

			List<Predicate> predicados = new ArrayList<>();

			if (tipoItemId != null) {
				Path<Long> campoId = root.get("id");
				Predicate predicadoId = builder.equal(campoId, tipoItemId);
				predicados.add(predicadoId);
			}

			if (StringUtils.hasText(tipoItem)) {
				Path<String> campoTipoItem = root.<String>get("tipoItem");
				Predicate predicadoTipoItem = builder.like(campoTipoItem, "%" + tipoItem + "%");
				predicados.add(predicadoTipoItem);
			}
			
			if (StringUtils.hasText(descricao)) {
				Path<String> campoDescricao = root.<String>get("descricao");
				Predicate predicadoDescricao = builder.like(campoDescricao, "%" + descricao + "%");
				predicados.add(predicadoDescricao);
			}

			return builder.and(predicados.toArray(new Predicate[0]));
		};
	}
}
