package br.com.detrango.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Entity
@Table(	name = "SAP_TIPO_ITEM",
uniqueConstraints = { 
	@UniqueConstraint(name = "IDX_DESC_TIPO_ITEM", columnNames = "DESC_TIPO_ITEM")
})
public class TipoItem {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_TIPO_ITEM", nullable = false)
    private long id;
	
	@Column(name = "TIPO_ITEM", length=10, nullable = false)
	private String tipoItem;
	
	@Column(name = "DESC_TIPO_ITEM", length=100, nullable = false)
	private String descricao;

}
