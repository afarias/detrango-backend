package br.com.detrango.business;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import br.com.detrango.model.TipoItem;
import br.com.detrango.model.filter.TipoItemFilter;
import br.com.detrango.repository.TipoItemRepository;

@Component
public class TipoItemBO {
	
	@Autowired
	private TipoItemRepository tipoItemRepository;

	public TipoItem salvar(TipoItem tipoItem) {
		return tipoItemRepository.save(tipoItem);
	}

	public Page<TipoItem> listar(TipoItemFilter filter, Pageable pageble) {
		return tipoItemRepository.findAll(filter.toSpec(), pageble);
	}

	public Optional<TipoItem> findById(long id) {
		return tipoItemRepository.findById(id);
	}

	public void apagar(Long id) {
		tipoItemRepository.deleteById(id);
	}

}
