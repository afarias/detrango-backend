# API Detran GO

# Tecnologias

- [X] *Spring Boot (v2.6.2)*
- [X] *JAVA 15*
- [X] *Multi Context (prod, dev) Spring Profile*
- [X] *Mysql*
- [X] *Lombok*
- [X] *Java Persistence API - JPA*
- [X] *Sprint Security (RULES and Permissions)*
- [X] *Autenticação Json Web Token (JWT)*
- [X] *Swagger Documentation*


---

## Instalação

> Caso possuir o maven instalado localmente substituir `./mvnw` por `mvn` 

```shell
# Clone o repositório na branch master e acesse o diretório.
$ git@gitlab.com:afarias/detrango-backend.git

# Buildar para produção
$ ./mvnw clean package -DskipTests


```

## Variáveis de ambiente

| **Descrição**                               | **parâmetro**                          | **Valor padrão**                  |
| ------------------------------------------- | -------------------------------------- | -------------------------         |
| contexto da aplicação                       | `server.servlet.context-path`                             | detrango-backend/               |
| porta da aplicação                          | `server.port`                                 | 8090                              |
| url do banco                                | `spring.datasource.url`                               | `jdbc:mysql://localhost:3306/detrango?createDatabaseIfNotExist=true&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC` |
| nome de usuário (banco)                     | `spring.datasource.username`                          | root                |
| senha do usuário (banco)                    | `spring.datasource.password`                          |                 |
| mostrar sql na saida                        | `show-sql`                             | false                             |
| 

## Documentação do Swagger
Disponível em: [http://localhost:8090/detrango-backend/swagger-ui/](http://localhost:8090/detrango-backend/swagger-ui/)